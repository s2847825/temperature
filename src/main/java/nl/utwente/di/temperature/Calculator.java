package nl.utwente.di.temperature;

import java.util.HashMap;

public class Calculator {

    public double getConvertTemp(String isbn) {
        int input = Integer.parseInt(isbn);
        double result = ((input * 5)/9) + 32;
        return result;
    }
}
