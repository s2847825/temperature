package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** *  Tests the Quoter */
public class TestTemperature {

    @Test
    public void testBook1() throws Exception {
        Calculator calculator = new Calculator();
        double fahrenheit = calculator.getConvertTemp("0");
        Assertions.assertEquals(32.0, fahrenheit, 0.0, "Conversion of 0 C to Fahrenheit");
    }
}
